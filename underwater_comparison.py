import enum
from inekf import SE3, InEKF, ERROR, InertialProcess, DepthSensor, DVLSensor
from helpers.qekf import QEKF, InertialProcessSE3QEKF, VelInertialSE3QEKF, DepthSE3QEKF
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import multivariate_normal
from helpers.misc import setup_figure
from tqdm import tqdm

np.set_printoptions(suppress=True)

DO_QEKF = True

#### HELPERS
def w(cov):
    s = cov.shape[0]
    if np.all(cov==0):
        return np.zeros(s)
    else:
        return multivariate_normal(np.zeros(s), cov=cov)

def make_filter(start, error, pm, **mm):
    if "inekf" in pm.__class__.__bases__[0].__module__:
        filter = InEKF(pm, start, error)
    else:
        filter = QEKF(pm, start, error)

    for name, model in mm.items():
        filter.addMeasureModel(name, model)

    return filter

def run_filter(filter, us, **measurements):
    result = [SE3[2,6](filter.state)]

    for i in range(len(us)):
        state = filter.predict(us[i], dt)
        
        for name, val in measurements.items():
            state = filter.update(name, val[i])

        result.append(state)

    return result

def get(f, dim):
    match dim:
        case "t1":
            return np.array([-np.arcsin(i.R.mat[2,0]) for i in f])*180/np.pi
        case "t2":
            return np.array([np.arctan2(i.R.mat[2,1], i.R.mat[2,2]) for i in f])*180/np.pi
        case "t3":
            return np.array([np.arctan2(i.R.mat[1,0], i.R.mat[0,0]) for i in f])*180/np.pi
        case "vx":
            return np.array([i.inverse[0][0] for i in f])
        case "vy":
            return np.array([i.inverse[0][1] for i in f])
        case "vz":
            return np.array([i.inverse[0][2] for i in f])
        case "px":
            return np.array([i.inverse[1][0] for i in f])
        case "py":
            return np.array([i.inverse[1][1] for i in f])
        case "pz":
            return np.array([i[1][2] for i in f])
        case "wx":
            return np.array([i.aug[0] for i in f])
        case "wy":
            return np.array([i.aug[1] for i in f])
        case "wz":
            return np.array([i.aug[2] for i in f])
        case "ax":
            return np.array([i.aug[3] for i in f])
        case "ay":
            return np.array([i.aug[4] for i in f])
        case "az":
            return np.array([i.aug[5] for i in f])
        case _:
            print("Invalid dim")

def mah(result, gt, error):
    es = np.zeros((len(gt), 8))
    match error:
        case ERROR.RIGHT:
            for i, (r, g) in enumerate(zip(result, gt)):
                es[i,:5] = (r @ g.inverse).log
                es[i,5:8] = g.aug - r.aug

        case ERROR.LEFT:
            for i, (r, g) in enumerate(zip(result, gt)):
                es[i,:5] = (g.inverse @ r ).log
                es[i,5:8] = g.aug - r.aug

        case None:
            for i, (r,g) in enumerate(zip(result, gt)):
                temp = np.zeros(8)
                es[i,0] = (g.R @ r.R.inverse).log
                es[i,1:3] = g[0] - r[0]
                es[i,3:5] = g[1] - r[1]
                es[i,5:8] = g.aug - r.aug

    c = np.zeros(len(gt))
    for i, (r, e) in enumerate(zip(result, es)):
        c[i] = e.T@np.linalg.inv(r.cov)@e

    return np.sqrt(c)
    

#### SETUP
std = np.diag( np.array([30*np.pi/180, 30*np.pi/180, 30*np.pi/180,
                        2, 2,2,
                        1, 1, 1,
                        0.1, 0.1, 0.1,
                        0.1, 0.1, 0.1])**2 )
Q = np.diag(np.array([2*np.pi/180, 2*np.pi/180, 2*np.pi/180,
                    0.01, 0.01, 0.01,
                    0, 0, 0,
                    0.01, 0.01, 0.01,
                    0.01, 0.01, 0.01])**2)
std_mm = .001
M = np.diag(np.array([std_mm**2, std_mm**2, std_mm**2, 0, 0]))
dt = .01
tt = 0.5
n = int(tt/dt)
num = 50

b_vel = np.array([0,0,0,-1,0])
b_depth = np.array([0,0,0,0,1])

op = InertialProcess()
op.setGyroNoise( np.sqrt(Q[0,0]) )
op.setAccelNoise( np.sqrt(Q[3,3]) )
op.setGyroBiasNoise( np.sqrt(Q[9,9]) )
op.setAccelBiasNoise( np.sqrt(Q[12,12]) )
mm_vel = DVLSensor()
mm_vel.setNoise(np.sqrt(M[0,0]), 0)
mm_depth = DepthSensor(np.sqrt(M[0,0]))

op_qekf = InertialProcessSE3QEKF(Q)
mm_q_vel = VelInertialSE3QEKF(M[:3,:3])
mm_q_depth = DepthSE3QEKF(M[:1,:1])


start = np.zeros(15)
x0 = SE3[2,6](start,std)

# Make biases
bias_w = np.cumsum(np.random.multivariate_normal([0,0,0], Q[9:12,9:12], n), axis=0)
bias_a = np.cumsum(np.random.multivariate_normal([0,0,0], Q[12:15,12:15], n), axis=0)
bias = np.hstack((bias_w, bias_a))

def u_func(t, noise=True):
    cov = Q[:6,:6] if noise else np.zeros((6,6))
    return t.reshape(-1,1)*np.zeros(6) + np.array([np.pi, 0.1, 0.1, 1, 1, -10]) + w(cov)


#### GATHER GROUND TRUTH DATA
ts = np.linspace(0, tt, n)
us = u_func(ts, noise=False)
gt = [x0]
for u, b in zip(us, bias):
    state = op.f(u, dt, gt[-1])
    state.aug = b
    gt.append(state)

#### GATHER NOISY DATA
us = u_func(ts, noise=True) + bias
zs_depth = [x.mat[2:3,4]           + w(M[0:1,0:1]) for x in gt]
zs_vel = [x.inverse.mat@b_vel  + w(M) for x in gt]


#### SETUP FIGURE
c = setup_figure()
rows = 4 if DO_QEKF else 2
height = 7.5 if DO_QEKF else 3.5
fig, ax = plt.subplots(rows, 15, sharex=True, sharey=False, figsize=(20,height), constrained_layout=True)

ax[0,0].set_ylabel("Left Filter", fontweight='bold')
ax[1,0].set_ylabel("Right Filter", fontweight='bold')
if DO_QEKF:
    ax[2,0].set_ylabel("Quat. Left Filter", fontweight='bold')
    ax[3,0].set_ylabel("Quat. Right Filter", fontweight='bold')

# Set titles
titles = [r'$\theta_1$ (deg)', r'$\theta_2$ (deg)', r'$\theta_3$ (deg)', 
        '$p_x$ (m)', '$p_y$ (m)', '$p_z$ (m)',
        '$v_x$ (m/s)', '$v_y$ (m/s)', '$v_z$ (m/s)',
        '$b^{\omega_x}$', '$b^{\omega_y}$', '$b^{\omega_z}$',
        '$b^{a_x}$', '$b^{a_y}$', '$b^{a_z}$']
for i, t in enumerate(titles):
    ax[0,i].set_title(t)


#### RUN InEKF
t = np.linspace(0,tt,n+1)
for i in tqdm(range(num)):

    off = multivariate_normal(np.zeros(15), std)
    off[5:] = 0
    start = x0@SE3[2,6].exp(off)
    riekf = make_filter(start, ERROR.RIGHT, op, depth=mm_depth, vel=mm_vel)
    liekf = make_filter(start, ERROR.LEFT, op, depth=mm_depth, vel=mm_vel)
    rqekf = make_filter(start, ERROR.RIGHT, op_qekf, depth=mm_q_depth, vel=mm_q_vel)
    lqekf = make_filter(start, ERROR.LEFT, op_qekf, depth=mm_q_depth, vel=mm_q_vel)

    lx = run_filter(liekf, us, vel=zs_vel, depth=zs_depth)
    rx = run_filter(riekf, us, vel=zs_vel, depth=zs_depth)
    results = [lx, rx]
    if DO_QEKF:
        lqx = run_filter(lqekf, us, vel=zs_vel, depth=zs_depth)
        rqx = run_filter(rqekf, us, vel=zs_vel, depth=zs_depth)
        results += [lqx, rqx]

    vals = ["t1", "t2", "t3", "px", "py", "pz", "vx", "vy", "vz", "wx", "wy", "wz", "ax", "ay", "az"]
    for i, v in enumerate(vals):
        for j, r in enumerate(results):
            ax[j,i].plot(t, get(r, v), lw=0.5)

# plot ground truth
vals = ["t1", "t2", "t3", "px", "py", "pz", "vx", "vy", "vz", "wx", "wy", "wz", "ax", "ay", "az"]
for i, v in enumerate(vals):
    for j in range(rows):
        ax[j,i].plot(t, get(gt, v), 'k--', lw=2)

for i in range(15):
    y_lower_lim = min(ax[j,i].get_ylim()[0] for j in range(rows))
    y_upper_lim = max(ax[j,i].get_ylim()[1] for j in range(rows))
    for j in range(rows):
        ax[j,i].set_ylim([y_lower_lim, y_upper_lim])

# plt.savefig("figures/imperfect_comparison.pdf", dpi=200)
plt.show()
