import enum
from pickle import FALSE
from inekf import SE2, InEKF, ERROR, MeasureModel
from helpers.qekf import QEKF, InertialProcessQEKF, MagInertialQEKF, GPSInertialQEKF, VelInertialQEKF
from helpers.imperfect import InertialProcess
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import multivariate_normal
from helpers.misc import setup_figure
from tqdm import tqdm
import scipy.stats

np.set_printoptions(suppress=True)

DO_QEKF = False
PLOT_BIAS = True

#### HELPERS
def w(cov):
    s = cov.shape[0]
    if np.all(cov==0):
        return np.zeros(s)
    else:
        return multivariate_normal(np.zeros(s), cov=cov)

def make_filter(start, error, pm, **mm):
    if "inekf" in pm.__class__.__bases__[0].__module__:
        filter = InEKF(pm, start, error)
    else:
        filter = QEKF(pm, start, error)

    for name, model in mm.items():
        filter.addMeasureModel(name, model)

    return filter

def run_filter(filter, us, **measurements):
    result = [SE2[2,3](filter.state)]

    for i in range(len(us)):
        state = filter.predict(us[i], dt)
        
        for name, val in measurements.items():
            state = filter.update(name, val[i])

        result.append(state)

    return result

def get(f, dim):
    match dim:
        case "theta":
            return np.array([i.R.log[0] for i in f])*180/np.pi
        case "vx":
            return np.array([i[0][0] for i in f])
        case "vy":
            return np.array([i[0][1] for i in f])
        case "px":
            return np.array([i[1][0] for i in f])
        case "py":
            return np.array([i[1][1] for i in f])
        case "w":
            return np.array([i.aug[0] for i in f])
        case "ax":
            return np.array([i.aug[1] for i in f])
        case "ay":
            return np.array([i.aug[2] for i in f])
        case _:
            print("Invalid dim")

def mah(result, gt, error):
    es = np.zeros((len(gt), 8))
    match error:
        case ERROR.RIGHT:
            for i, (r, g) in enumerate(zip(result, gt)):
                es[i] = (r @ g.inverse).log

        case ERROR.LEFT:
            for i, (r, g) in enumerate(zip(result, gt)):
                es[i] = (g.inverse @ r ).log

        case None:
            for i, (r,g) in enumerate(zip(result, gt)):
                temp = np.zeros(8)
                es[i,0] = (g.R @ r.R.inverse).log
                es[i,1:3] = g[0] - r[0]
                es[i,3:5] = g[1] - r[1]
                es[i,5:8] = g.aug - r.aug

    c = np.zeros(len(gt))
    for i, (r, e) in enumerate(zip(result, es)):
        c[i] = e.T@np.linalg.inv(r.cov)@e

    return np.sqrt(c)
    

#### SETUP
cov = np.diag( np.array([10*np.pi/180, 
                        4, 4,
                        1, 1,
                        0.01,
                        0.01, 0.01]) )
Q = np.diag(np.array([2*np.pi/180,
                    0.01, 0.01, 
                    0, 0,
                    0.01,
                    0.01, 0.01])**2)
std_mm = .1
M = np.diag(np.array([std_mm**2, std_mm**2, 0, 0]))
dt = .01
tt = 0.5
n = int(tt/dt)
num = 100

b_vel = np.array([0,0,-1,0])
b_gps = np.array([0,0,0,1])
b_mag = np.array([1,0,0,0])

op = InertialProcess(Q)
mm_gps = MeasureModel[SE2[2,3]](b_gps, M[:2,:2], ERROR.LEFT) 
mm_vel = MeasureModel[SE2[2,3]](b_vel, M[:2,:2], ERROR.RIGHT)
mm_mag = MeasureModel[SE2[2,3]](b_mag, M[:2,:2], ERROR.RIGHT)

op_qekf = InertialProcessQEKF(Q)
mm_q_gps = GPSInertialQEKF(M[:2,:2])
mm_q_vel = VelInertialQEKF(M[:2,:2])
mm_q_mag = MagInertialQEKF(M[:2,:2]/8)


start = np.zeros(8)
x0 = SE2[2,3](start,cov)

# Make biases
bias_w = np.cumsum(np.random.normal(0, np.sqrt(Q[5,5]), n))
bias_a = np.cumsum(np.random.multivariate_normal([0,0], Q[6:8,6:8], n), axis=0)
bias = np.hstack((bias_w.reshape(-1,1), bias_a))

def u_func(t, noise=True):
    cov = Q[:3,:3] if noise else np.zeros((3,3))
    return t.reshape(-1,1)*np.zeros(3) + np.array([np.pi*0.75, 5, -1]) + w(cov)


#### GATHER GROUND TRUTH DATA
ts = np.linspace(0, tt, n)
us = u_func(ts, noise=False)
gt = [x0]
for u, b in zip(us, bias):
    state = op.f(u, dt, gt[-1])
    state.aug = b
    gt.append(state)


#### GATHER NOISY DATA
us = u_func(ts, noise=True) + bias
zs_gps = [x.mat@b_gps           + w(M) for x in gt]
zs_vel = [x.inverse.mat@b_vel  + w(M) for x in gt]
zs_mag = [x.inverse.mat@b_mag + w(M/8) for x in gt]

#### SETUP FIGURE
c = setup_figure()
rows = 3 if DO_QEKF else 2
height = 4.5 if DO_QEKF else 3
columns = 9 if PLOT_BIAS else 5
fig, ax = plt.subplots(rows, columns, sharex=True, sharey=False, figsize=(8.5,height), constrained_layout=True)

ax[0,0].set_ylabel("Left Filter", fontweight='bold')
ax[1,0].set_ylabel("Right Filter", fontweight='bold')
if DO_QEKF:
    ax[2,0].set_ylabel("Quat. Filter", fontweight='bold')

# Set titles
titles = [r'$\theta$ (deg)', '$p_x$ (m)', '$p_y$ (m)', '$v_x$ (m/s)', '$v_y$ (m/s)']
if PLOT_BIAS:
    titles += ['$b^{\omega}$', '$b^{a_x}$', '$b^{a_y}$', "Mah. Dist."]
for i, t in enumerate(titles):
    ax[0,i].set_title(t)

for i in range(rows):
    for j in range(columns):
        ax[i,j].tick_params(axis='both', which='major', pad=-1)


vals = ["theta", "px", "py", "vx", "vy"]
if PLOT_BIAS:
    vals += ["w", "ax", "ay"]

#### RUN InEKF
types = [ERROR.LEFT, ERROR.RIGHT, None]
t = np.linspace(0,tt,n+1)
for i in tqdm(range(num)):

    off = multivariate_normal(np.zeros(8), cov)
    off[5:] = 0
    start = x0@SE2[2,3].exp(off)
    riekf = make_filter(start, ERROR.RIGHT, op, gps=mm_gps, vel=mm_vel, mag=mm_mag)
    liekf = make_filter(start, ERROR.LEFT, op, gps=mm_gps, vel=mm_vel, mag=mm_mag)
    qekf = make_filter(start, None, op_qekf, gps=mm_q_gps, vel=mm_q_vel, mag=mm_q_mag)

    meas_to_use = {"gps": zs_gps, "vel": zs_vel, "mag": zs_mag}
    lx = run_filter(liekf, us, **meas_to_use)
    rx = run_filter(riekf, us, **meas_to_use)
    results = [lx, rx]
    if DO_QEKF:
        qx = run_filter(qekf, us, **meas_to_use)
        results.append(qx)

    for j, r in enumerate(results):
        for i, v in enumerate(vals):
            ax[j,i].plot(t, get(r, v), lw=0.5)

        ax[j,-1].plot(t, mah(r, gt, types[j]), lw=0.5)

# plot ground truth
for j in range(rows):
    for i, v in enumerate(vals):
        ax[j,i].plot(t, get(gt, v), 'k--', lw=2)

    ax[j,-1].plot(t, t*0 + scipy.stats.chi2.ppf(0.99, 8), 'k--', lw=2)

for i in range(columns):
    y_lower_lim = min(ax[j,i].get_ylim()[0] for j in range(rows))
    y_upper_lim = max(ax[j,i].get_ylim()[1] for j in range(rows))
    y_lim = [y_lower_lim, y_upper_lim]
    if i in [0]:
        y_lim = [-24, 74]
    if i in [1,2]:
        y_lim = [-0.4, 0.9]
    if i in [3]:
        y_lim = [-0.4, 2.9]
    if i in [4]:
        y_lim = [-0.6, 1.4]
    for j in range(rows):
        ax[j,i].set_ylim(y_lim)

plt.savefig("figures/imperfect_comparison.pdf", dpi=200)
plt.show()
