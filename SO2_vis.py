import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from inekf import SE2
from helpers.misc import setup_figure, set_axes_equal
from helpers.Annotation3D import *

from matplotlib.lines import Line2D
from matplotlib.patches import Patch

# Add text at a specific point
def annotate(x, y, name, ax, xytext=(8,11)):
    ax.annotate(name, (x, y), xytext=xytext, textcoords='offset points', size='large',
                color='white', bbox=dict(boxstyle="round", fc="black"))

def plot_man(loc, theta, c, lw=2.5, ax=None, show_pt=True, show_arrow=True, name=None, name_loc=(8,11), zorder=2):
    # want equal spacing in all our plots
    step = 0.01
    if theta > 0:
        t = np.arange(theta, 0, -step)
    elif theta < 0:
        t = np.arange(theta, 0, step)
    else:
        t = [0]

    # make our coordinates
    if loc == "g":
        x = np.cos(t)
        y = np.sin(t)
    if loc == "a":
        x = np.ones_like(t)
        y = t

    if ax is None:
        ax = plt.gca()

    ax.plot(x, y, color=c, lw=lw, zorder=zorder)

    if show_pt:
        ax.scatter(x[0], y[0], color=c, lw=lw, zorder=zorder)
    if name is not None:
        annotate(x[0], y[0], name, ax=ax, xytext=name_loc)
    if show_arrow:
        idx = 3
        ax.annotate("",
            xy=(x[idx], y[idx]), xycoords='data',
            xytext=(x[idx+1], y[idx+1]), textcoords='data',
            arrowprops=dict(arrowstyle="-|>", color=c, mutation_scale=20
                            ),
            )

#### PLOT
c = setup_figure()
fig, ax = plt.subplots(1, 1, figsize=(4,4))

# plot structures
lw_man = 1
plot_man("a",     1.2, c=c[1], show_pt=False, show_arrow=False, lw=lw_man)
plot_man("a",    -1.2, c=c[1], show_pt=False, show_arrow=False, lw=lw_man)
plot_man("g", 2*np.pi, c=c[0], show_pt=False, show_arrow=False, lw=lw_man)

# Plot 2 different points
eta = np.pi/3
X = -np.pi*9/12
plot_man("g", eta, c=c[0], name="$\eta$", name_loc=(-18,-5), zorder=10)
plot_man("g",   X, c=c[0], name="$X$")
plot_man("a", eta, c=c[1], name="$\\xi^{\wedge}$", name_loc=(10,-2), zorder=5)

# plot identity
plot_man("g", 0, c='k', zorder=10, name="$I$", show_arrow=False)

# plot exp
l = np.linspace([1,eta], [np.cos(eta), np.sin(eta)])
ax.annotate("",
            xy=(np.cos(eta), np.sin(eta)), xycoords='data',
            xytext=(1, eta), textcoords='data',
            arrowprops=dict(arrowstyle="-|>", color=c[3], lw=2, mutation_scale=13,
                            connectionstyle="arc3,rad=0.2", joinstyle='miter'
                            ),
            )

# plot tangent space of X
angle = X - np.pi/2
l = 0.7
x, y = np.cos(X), np.sin(X)
pts = np.linspace([x+l*np.cos(angle), y+l*np.sin(angle)], [x-l*np.cos(angle), y-l*np.sin(angle)])
plt.plot(pts[:,0], pts[:,1], ls="--", c=c[1], lw=lw_man, zorder=1)

# Add legend
custom_lines = [
                Line2D([0], [0], color=c[0], lw=lw_man),
                Line2D([0], [0], color=c[1], lw=lw_man),
                Line2D([0], [0], color=c[1], ls="--", lw=lw_man),
                Line2D([0], [0], color=c[3], lw=1.5)
            ]
names = ["$SO(2)$", "$\mathfrak{so}(2)$", "$T_X SO(2)$", "$\exp$"]
leg = ax.legend(custom_lines, 
            names, 
            fontsize="medium", 
            ncol=4, 
            loc='lower center',
            bbox_to_anchor=(0.48,-0.2),
            fancybox=True,
            shadow=True,
        )

ax.set_xlim([-1.3, 1.4])
ax.set_aspect('equal', adjustable='box')
plt.subplots_adjust(left=0, right=1.08, bottom=0.17, top=0.99)
plt.savefig(f'figures/so2.pdf', dpi=200)
plt.show()
