import enum
import numpy as np
from inekf import SE3, ERROR, InertialProcess
from helpers.qekf import InertialProcessSE3QEKF
import matplotlib.pyplot as plt
from tqdm import tqdm
# np.set_printoptions(edgeitems=30, linewidth=100000, suppress=True)


def calc_error(true, estimate, error):
    match error:
        case "Right":
            return (estimate@true.inverse).log
        case "Left":
            return (true.inverse@estimate).log
        case "QEKF":
            e = np.zeros(15)
            e[:3] = (true.R.inverse@estimate.R).log
            e[3:6] = true[0] - estimate[0]
            e[6:9] = true[1] - estimate[1]
            e[9:15] = true.aug - estimate.aug

            return e

num_plot = 20
n = 1000
dt = .01

result = np.zeros((num_plot, 3))
for iter, scale in tqdm(enumerate(np.linspace(0, 1, num_plot))):
    off = np.zeros(15)
    off[:3] = np.pi/2 * scale

    true = SE3[2,6]()
    estimate = SE3[2,6].exp(off)

    types = ["Right", "Left", "QEKF"]
    errors = [calc_error(true, estimate, e) for e in types]

    controls = np.random.normal(loc=0, scale=1, size=(n,6))
    inertial_iekf = InertialProcess()
    inertial_qekf = InertialProcessSE3QEKF(np.eye(15))

    for i, u in enumerate(controls):
        w = np.random.normal(scale=0.1, size=6)
        errors[0] = inertial_iekf.makePhi(u+w, dt, estimate, ERROR.RIGHT)@errors[0]
        errors[1] = inertial_iekf.makePhi(u+w, dt, estimate, ERROR.LEFT)@errors[1]
        errors[2] = inertial_qekf.makePhi(u+w, dt, estimate, ERROR.RIGHT)@errors[2]

        true = inertial_iekf.f(u, dt, true)
        estimate = inertial_iekf.f(u, dt, estimate)
        
    gt = [calc_error(true, estimate, e) for e in types]

    # for i in range(3):
    #     print(types[i], "\t", np.linalg.norm(gt[i] - errors[i]))

    for i in range(3):
        result[iter,i] = np.linalg.norm(gt[i] - errors[i])

for i in range(3):
    plt.plot(np.linspace(0,1,num_plot), result[:,i], label=types[i])
plt.legend()
plt.show()