import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from inekf import SE2
from helpers.misc import setup_figure, set_axes_equal
from helpers.Annotation3D import *

from matplotlib.lines import Line2D
from matplotlib.patches import Patch

def w(c1):
    c1._facecolors2d = c1._facecolor3d
    c1._edgecolors2d = c1._edgecolor3d

def f(x):
    return np.linalg.norm(x.mat)**2

# line between points in the group
def plot_line_g_g(start, end, ax, name=None, *args, **kwargs):
    dom = np.linspace(start.log, end.log, 50)

    X = dom[:,1]
    Y = dom[:,2]
    Z = np.array([f(SE2.exp(d)) for d in dom])
    
    ax.plot(X, Y, Z, *args, **kwargs)

    dom = np.vstack((X,Y,Z)).T
    ax.arrow3D(*dom[-2], *(dom[-1]-dom[-2]), mutation_scale=15, *args, **kwargs)

    # if name != None:
        # annotate(*dom[dom.shape[0]//2], name, ax, xytext=(-5,-5))

# line from a point in group to it's log in the tangent space
def plot_line_g_tan(state, ax, *args, **kwargs):
    _, x, y = state.log
    start = [x, y, f(state)]
    end   = [x, y, f(SE2())]
    dom = np.linspace(start, end, 50)
    X, Y, Z = dom.T

    ax.plot(X, Y, Z, '--', *args, **kwargs)

    ax.arrow3D(*dom[-2], *(dom[-1]-dom[-2]), mutation_scale=15, *args, **kwargs)

# plots point in group and respective point in tangent space
def plot_error(E, name, tan_name, ax, alpha=1, color1="k", color2="k", *args, **kwargs):
    _, x, y = E.log
    z = f(E)
    ax.scatter(x, y, z, marker='o', alpha=alpha, color=color1, *args, **kwargs)
    annotate(x, y, z, name, ax)

    z = f(SE2())
    ax.scatter(x, y, z, marker='o', alpha=alpha, color=color2, *args, **kwargs)
    annotate(x, y, z, tan_name, ax)

# plots point and it's inverse in the group
def plot_state(X, name, inv_name, ax, alpha=1, *args, **kwargs):
    _, x, y = X.log
    z = f(X)
    ax.scatter(x, y, z, marker='o', alpha=alpha, *args, **kwargs)
    annotate(x, y, z, name, ax)

    _, x, y = (~X).log
    z = f(~X)
    ax.scatter(x, y, z, marker='x', alpha=alpha, *args, **kwargs)
    annotate(x, y, z, inv_name, ax)

# plot a single point in the group
def plot_pt(X, name, ax, alpha=1, *args, **kwargs):
    _, x, y = X.log
    z = f(X)
    ax.scatter(x, y, z, marker='o', alpha=alpha, *args, **kwargs)
    annotate(x, y, z, name, ax)

# Add text at a specific point
def annotate(x, y, z, name, ax, xytext=(8,-13)):
    ax.annotate3D(name, (x, y, z), xytext=xytext, textcoords='offset points', size='medium',
                color='white', bbox=dict(boxstyle="round", fc="black"))

##### PARAMETERS
n = 25 # density of surface
s = 20 # marker size
lw = 1.5
theta = np.pi/4
X    = SE2.exp([theta, 1.5, 1.5])
Xhat = SE2.exp([theta, 2, 0])

##### SETUP MANIFOLD
x = np.linspace(-2,2, n)
y = np.linspace(-2,2, n)
XS, YS = np.meshgrid(x, y)
Z = np.array([[f(SE2.exp([theta, a,b])) for a,b in zip(A_S, B_S)] for A_S, B_S in zip(XS, YS)])

##### CALCULATE ERROR
R = Xhat@~X
L = ~X@Xhat

#### PLOT
c = setup_figure()
fig = plt.figure(figsize=(4,4.15))
ax = fig.add_subplot(projection='3d')

# plot surfaces
w(ax.plot_surface(XS, YS,                         Z, alpha=0.2, color=c[9], label="Lie Group",     shade=True, linewidth=0))
w(ax.plot_surface(XS, YS, np.zeros_like(Z)+f(SE2()), alpha=0.2, color=c[7], label="Tangent Space", shade=True, linewidth=0))

# plot all points + labels
plot_pt(SE2(), "$I$", ax, s=s, color="k")
plot_error(R, r"$\eta^r$", r"$\xi^r$", ax, s=s, color1=c[0], color2=c[1])
plot_error(L, r"$\eta^l$", r"$\xi^l$", ax, s=s, color1=c[0], color2=c[1])
plot_line_g_tan(R, ax,                    color=c[3])
plot_line_g_tan(L, ax,                  color=c[3])

# plot lines/arrows/labels
plot_line_g_g(SE2([theta,0,0]), Xhat, ax, "$\hat{X}$", color=c[0], lw=lw)
plot_line_g_g(              ~X,  L, ax, "$\hat{X}$", color=c[0], lw=lw)

plot_line_g_g(SE2([theta,0,0]), ~X, ax, "$X^{-1}$",  color=c[2], lw=lw)
plot_line_g_g(            Xhat,    R, ax, "$X^{-1}$",  color=c[2], lw=lw)

# set misc properties
ax.zaxis.set_rotate_label(False)
ax.view_init(elev=33, azim=-56)
ax.invert_zaxis()
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_zticklabels([])

# Setup legend
custom_lines = [
                Patch(facecolor=c[9], alpha=0.8),
                Patch(facecolor=c[7], alpha=0.8),
                Line2D([0], [0], color=c[0], lw=lw),
                Line2D([0], [0], color=c[2], lw=lw),
                Line2D([0], [0], color=c[3], lw=lw, linestyle='--'),
            ]
names = ["$\mathcal{G}$", "$\mathfrak{g}$", "$\hat{X}$", "$X^{-1}$", "$\mathcal{G} \; \\to \; \mathfrak{g}$"]
leg = ax.legend(custom_lines, 
            names, 
            fontsize="medium", 
            ncol=3, 
            loc='lower center',
            bbox_to_anchor=(0.5,-0.08),
            fancybox=True,
            shadow=True,
        )

# save and show
plt.subplots_adjust(left=0, right=1, bottom=0, top=1.13)
plt.savefig(f'figures/lie_group.pdf', dpi=200)
plt.show()
