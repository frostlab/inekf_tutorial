import enum
from inekf import SE2, OdometryProcess, InEKF, ERROR, MeasureModel
from helpers.qekf import QEKF, OdometryProcessQEKF, GPSOdometryQEKF, MagOdometryQEKF, LandOdometryQEKF
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import multivariate_normal
from helpers.misc import setup_figure
from tqdm import tqdm
import scipy.stats

np.set_printoptions(suppress=True)

DO_QEKF = False

#### HELPERS
def w(cov):
    s = cov.shape[0]
    if np.all(cov==0):
        return np.zeros(s)
    else:
        return multivariate_normal(np.zeros(s), cov=cov)

def make_filter(start, error, pm, **mm):
    if error is None:
        filter = QEKF(pm, start)
    else:
        filter = InEKF(pm, start, error)

    for name, model in mm.items():
        filter.addMeasureModel(name, model)
    return filter

def run_filter(filter, us, **measurements):
    result = [SE2(filter.state)]

    for i in range(len(us)):
        state = filter.predict(us[i], dt)
        
        for name, val in measurements.items():
            state = filter.update(name, val[i])

        result.append(state)

    return result

def x(l, inv=False):
    if inv:
        return np.array([i.inverse[0][0] for i in l])
    else:
        return np.array([i[0][0] for i in l])

def y(l, inv=False):
    if inv:
        return np.array([i.inverse[0][1] for i in l])
    else:
        return np.array([i[0][1] for i in l])

def theta(l, inv=False):
    if inv:
        return np.unwrap(np.array([i.inverse.log[0] for i in l]))
    else:
        return np.unwrap(np.array([i.log[0] for i in l]))

def mah(result, gt, error):
    es = np.zeros((len(gt), 3))
    if error == ERROR.RIGHT:
        for i, (r, g) in enumerate(zip(result, gt)):
            es[i] = (r @ g.inverse).log

    elif error == ERROR.LEFT:
        for i, (r, g) in enumerate(zip(result, gt)):
            es[i] = (g.inverse @ r ).log

    elif error == None:
        for i, (r,g) in enumerate(zip(result, gt)):
            temp = np.zeros(3)
            temp[0] = (g.R @ r.R.inverse).log
            temp[1:] = g[0] - r[0]
            es[i] = temp

    c = np.zeros(len(gt))
    for i, (r, e) in enumerate(zip(result, es)):
        c[i] = e.T@np.linalg.inv(r.cov)@e
    # return 1 - np.exp(-c/2)
    # return stats.chi2.cdf(c, df=3)
    return np.sqrt(c)
    

#### SETUP
cov = np.diag( np.array([25*np.pi/180, 1, 1]) )
Q = np.diag(np.array([2*np.pi/180,.05,0.01]))
std_mm = .1
M = np.diag(np.array([std_mm**2, std_mm**2, 0]))
dt = .01
tt = 0.5
n = int(tt/dt)
num = 100

b_lm = np.array([1,1,1])
b_gps = np.array([0,0,1])
b_mag = np.array([1,0,0])

op = OdometryProcess(Q)
mm_gps = MeasureModel[SE2](b_gps, M[:2,:2], ERROR.LEFT) #GPSSensor(std_mm)
mm_lm = MeasureModel[SE2](b_lm, M[:2,:2], ERROR.RIGHT) #LandmarkSensor(b_lm[:2], std_mm)
mm_mag = MeasureModel[SE2](b_mag, M[:2,:2], ERROR.RIGHT) #CompasSensor(std_mm)

op_qekf = OdometryProcessQEKF(Q)
mm_q_gps = GPSOdometryQEKF(M[:2,:2])
mm_q_lm = LandOdometryQEKF(M[:2,:2], np.array([1,1]))
mm_q_mag = MagOdometryQEKF(M[:2,:2])

x0 = SE2(0,0,0,cov)
def u_func(t, noise=True):
    cov = Q*dt if noise else np.zeros((3,3))
    return np.array([np.pi, 1/.32, 0])*dt + w(cov)


#### GATHER GROUND TRUTH DATA
ts = np.linspace(0, tt, n)
us = [SE2(u_func(t, noise=False)) for t in ts]
gt = [x0]
for u in us:
    state = op.f(u, dt, gt[-1])
    gt.append(state)

#### GATHER NOISY DATA
us = [SE2(u_func(t, noise=True)) for t in ts]
zs_gps = [x.mat@b_gps           + w(M) for x in gt]
zs_lm =  [x.inverse.mat@b_lm  + w(M) for x in gt]
zs_mag = [x.inverse.mat@b_mag + w(M) for x in gt]

#### SETUP FIGURE
c = setup_figure()
rows = 3 if DO_QEKF else 2
height = 5.5 if DO_QEKF else 3.5
fig, ax = plt.subplots(rows, 8, sharex=True, sharey=False, figsize=(8.5,height), constrained_layout=True)

ax[0,0].set_ylabel("Left Filter", fontweight='bold')
ax[1,0].set_ylabel("Right Filter", fontweight='bold')
if DO_QEKF:
    ax[2,0].set_ylabel("Quat. Filter", fontweight='bold')

# ax[0,0].set_xlabel(" \n ")
ax[0,4].set_ylabel(" \n\nLeft Filter", fontweight='bold')
ax[1,4].set_ylabel(" \n\nRight Filter", fontweight='bold')
if DO_QEKF:
    ax[2,4].set_ylabel(" \n\nQuat. Filter", fontweight='bold')

fig.suptitle("    Left Measurements                                                    Right Measurements", fontweight='bold')

# Set titles
ax[0,1].set_title("y")
for i in range(2):
    ax[0,4*i].set_title('$x$ (m)')
    ax[0,4*i+1].set_title('$y$ (m)')
    ax[0,4*i+2].set_title(r'$\theta$ (rad)')
    ax[0,4*i+3].set_title(r'Mah. Dist.')

# Hook y axes together
for i in range(rows):
    ax[i,0].get_shared_y_axes().join(ax[i,0], *ax[i,0:3])
    ax[i,1].set_yticklabels([])
    ax[i,2].set_yticklabels([])

    ax[i,4].get_shared_y_axes().join(ax[i,4], *ax[i,4:7])
    ax[i,5].set_yticklabels([])
    ax[i,6].set_yticklabels([])

# pad labels
for i in range(rows):
    for j in range(8):
        ax[i,j].tick_params(axis='both', which='major', pad=-1)


#### RUN InEKF
t = np.linspace(0,tt,n+1)
for i in tqdm(range(num)):
    # run with left only
    start = x0@SE2(multivariate_normal(np.zeros(3), cov))
    riekf = make_filter(start, ERROR.RIGHT, op, gps=mm_gps, lm=mm_lm, mag=mm_mag)
    liekf = make_filter(start, ERROR.LEFT, op, gps=mm_gps, lm=mm_lm, mag=mm_mag)
    qekf = make_filter(start, None, op_qekf, gps=mm_q_gps, lm=mm_q_lm, mag=mm_q_mag)

    rx = run_filter(riekf, us, gps=zs_gps)
    lx = run_filter(liekf, us, gps=zs_gps)
    if DO_QEKF:
        qx = run_filter(qekf, us, gps=zs_gps)

    ax[0,0].plot(t, x(lx), lw=0.5)
    ax[0,1].plot(t, y(lx), lw=0.5)
    ax[0,2].plot(t, theta(lx), lw=0.5)
    ax[0,3].plot(t, mah(lx, gt, ERROR.LEFT), lw=0.5)

    ax[1,0].plot(t, x(rx), lw=0.5)
    ax[1,1].plot(t, y(rx), lw=0.5)
    ax[1,2].plot(t, theta(rx), lw=0.5)
    ax[1,3].plot(t, mah(rx, gt, ERROR.RIGHT), lw=0.5)

    if DO_QEKF:
        ax[2,0].plot(t, x(qx), lw=0.5)
        ax[2,1].plot(t, y(qx), lw=0.5)
        ax[2,2].plot(t, theta(qx), lw=0.5)
        ax[2,3].plot(t, mah(qx, gt, None), lw=0.5)


    # run with right only
    riekf = make_filter(start, ERROR.RIGHT, op, gps=mm_gps, lm=mm_lm, mag=mm_mag)
    liekf = make_filter(start, ERROR.LEFT, op, gps=mm_gps, lm=mm_lm, mag=mm_mag)
    qekf = make_filter(start, None, op_qekf, gps=mm_q_gps, lm=mm_q_lm, mag=mm_q_mag)

    rx = run_filter(riekf, us, lm=zs_lm, mag=zs_mag)
    lx = run_filter(liekf, us, lm=zs_lm, mag=zs_mag)
    if DO_QEKF:
        qx = run_filter(qekf, us, lm=zs_lm, mag=zs_mag)

    ax[0,4].plot(t, x(lx), lw=0.5)
    ax[0,5].plot(t, y(lx), lw=0.5)
    ax[0,6].plot(t, theta(lx), lw=0.5)
    ax[0,7].plot(t, mah(lx, gt, ERROR.LEFT), lw=0.5)

    ax[1,4].plot(t, x(rx), lw=0.5)
    ax[1,5].plot(t, y(rx), lw=0.5)
    ax[1,6].plot(t, theta(rx), lw=0.5)
    ax[1,7].plot(t, mah(rx, gt, ERROR.RIGHT), lw=0.5)

    if DO_QEKF:
        ax[2,4].plot(t, x(qx), lw=0.5)
        ax[2,5].plot(t, y(qx), lw=0.5)
        ax[2,6].plot(t, theta(qx), lw=0.5)
        ax[2,7].plot(t, mah(qx, gt, None), lw=0.5)

for i in [0,3,4,7]:
    y_lower_lim = min(ax[j,i].get_ylim()[0] for j in range(rows))
    y_upper_lim = max(ax[j,i].get_ylim()[1] for j in range(rows))
    if i in [3, 7]:
        y_upper_lim = 25
    for j in range(rows):
        ax[j,i].set_ylim([y_lower_lim, y_upper_lim])

# plot ground truth
for i in range(rows):
    for j in range(2):
        ax[i,4*j].plot(t, x(gt), 'k--', lw=2)
        ax[i,4*j+1].plot(t, y(gt), 'k--', lw=2)
        ax[i,4*j+2].plot(t, theta(gt), 'k--', lw=2)
        # ax[i,4*j+3].plot(t, np.zeros_like(t), 'k--', lw=2)
        ax[i,4*j+3].plot(t, t*0 + scipy.stats.chi2.ppf(0.99, 3), 'k--', lw=2)

plt.savefig("figures/odometry_comparison.pdf", dpi=200)
plt.show()
