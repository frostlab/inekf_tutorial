import numpy as np
from inekf import SE2, SO2, ProcessModel, MeasureModel, ERROR
from scipy.linalg import expm

class InertialProcess(ProcessModel[SE2[2,3], "Vec3"]):
    def __init__(self, Q=np.zeros((8,8))) -> None:
        super().__init__()
        self.Q = Q
        self.V = np.array([[0, -1],
                    [1, 0]])

    def f(self, u, dt, state):
        u_shifted = u - state.aug
        omega = u_shifted[:1]
        a = u_shifted[1:]
        R = state.R.mat
        v = state[0]
        p = state[1]

        S = np.eye(4)
        S[:2,:2] = R @ SO2.exp(omega*dt).mat
        S[:2,2]  = v + R@a*dt
        S[:2,3]  = p + v*dt + R@a*dt**2/2

        state.mat = S

        return SE2[2,3](state)

    def makePhi(self, u, dt, state, error):
        A = np.zeros((8,8))

        if error == ERROR.RIGHT:
            R = state.R.mat
            v_cross = self.V@state[0]
            p_cross = self.V@state[1]

            A[3:5,1:3] = np.eye(2)
            A[0,5] = -1
            A[1:3,5] = v_cross
            A[3:5,5] = p_cross
            A[3:5,6:8] = -R

            return np.eye(8) + A*dt + A@A*dt**2/2

        elif error == ERROR.LEFT:
            u_shifted = u - state.aug
            w_cross = SO2.wedge( u_shifted[0:1] )
            a_cross = self.V@u_shifted[1:3]

            A[1:3,0] = a_cross
            A[1:3,1:3] = -w_cross
            A[3:5,3:5] = -w_cross
            A[3:5,1:3] = np.eye(2)

            A[0,5] = -1
            A[1:3,6:8] = -np.eye(2)

            return expm(A*dt)

    def setGyroNoise(self, std):
        Q = self.Q
        Q[0,0] = std**2
        self.Q = Q

    def setAccelNoise(self, std):
        Q = self.Q
        Q[1:3,1:3] = np.eye(2)*std**2
        self.Q = Q

    def setGyroBiasNoise(self, std):
        Q = self.Q
        Q[5,5] = std**2
        self.Q = Q

    def setAccelBiasNoise(self, std):
        Q = self.Q
        Q[6:8,6:8] = np.eye(2)*std**2
        self.Q = Q