import numpy as np
from inekf import SE2, SE3, SO2, SO3, ERROR
from scipy.linalg import expm
np.set_printoptions(edgeitems=30, linewidth=100000)

#------------------- Odometry Models ----------------#
class OdometryProcessQEKF:
    def __init__(self, Q):
        self.Q = Q

    def f(self, u, dt, state):
        return state@u

    def makePhi(self, u, dt, state, error):
        t = u[0]
        V = np.array([[0, 1],
                    [-1, 0]])
        R = state.R.mat
        temp = -R@V@t
        A = np.array([[0, 0, 0],
                      [temp[0], 0, 0],
                      [temp[1], 0, 0]])
        return expm(A)


class GPSOdometryQEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        H = np.zeros((2,3))
        H[:,1:] = np.eye(2)
        return H

    def calcV(self, state, z):
        return z[:2] - state[0]

class MagOdometryQEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        H = np.zeros((2,3))
        H[1,0] = -1
        H = state.R.mat.T@H
        return H

    def calcV(self, state, z):
        return z[:2] - state.R.mat.T@[1,0]

class LandOdometryQEKF:
    def __init__(self, M, l):
        self.M = M
        self.l = l

    def H(self, state, error):
        V = np.array([[0, 1],
                    [-1, 0]])
        H = np.zeros((2,3))
        H[:,0] = V@(self.l - state[0])
        H[:,1:] = -np.eye(2)
        H = state.R.mat.T@H
        return H

    def calcV(self, state, z):
        return z[:2] - state.R.mat.T@(self.l - state[0])


#------------------- Inertial Models ----------------#
class InertialProcessQEKF:
    def __init__(self, Q):
        self.Q = Q
        self.V = np.array([[0, 1],
                           [-1, 0]])

    def f(self, u, dt, state):
        u_shifted = u - state.aug
        omega = u_shifted[:1]
        a = u_shifted[1:]
        R = state.R.mat
        v = state[0]
        p = state[1]

        S = np.eye(4)
        S[:2,:2] = R @ SO2.exp(omega*dt).mat
        S[:2,2]  = v + R@a*dt
        S[:2,3]  = p + v*dt + R@a*dt**2/2

        state.mat = S

        return SE2[2,3](state)

    def makePhi(self, u, dt, state, error):
        a = u[1:] - state.aug[1:]
        R = state.R.mat

        A = np.zeros((8,8))
        A[0,5] = -1
        A[1:3,0] = -R@self.V@a
        A[1:3,6:8] = -R
        A[3:5,1:3] = np.eye(2)

        return np.eye(8) + A*dt + A@A*dt**2/2 + A@A@A*dt**3/6


class GPSInertialQEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        H = np.zeros((2,8))
        H[:,3:5] = np.eye(2)
        return H

    def calcV(self, state, z):
        return z[:2] - state[1]

class MagInertialQEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        H = np.zeros((2,8))
        H[1,0] = -1
        H = state.R.mat.T@H
        return H

    def calcV(self, state, z):
        return z[:2] - state.R.mat.T@[1,0]

class VelInertialQEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        V = np.array([[0, 1],
                    [-1, 0]])
        H = np.zeros((2,8))
        H[:,0] = V@state[0]
        H[:,1:3] = np.eye(2)
        H = state.R.mat.T@H
        return H

    def calcV(self, state, z):
        return z[:2] - state.R.mat.T@state[0]



#------------------- SE3 Inertial Models ----------------#
class InertialProcessSE3QEKF:
    def __init__(self, Q):
        self.Q = Q

        self.g = np.array([0, 0, -9.81])


    def f(self, u, dt, state):
        u_shifted = u - state.aug
        omega = u_shifted[:3]
        a = u_shifted[3:]
        R = state.R.mat
        v = state[0]
        p = state[1]

        S = np.eye(5)
        S[:3,:3] = R @ SO3.exp(omega*dt).mat
        S[:3,3]  = v + (R@a + self.g)*dt
        S[:3,4]  = p + v*dt + (R@a + self.g)*dt**2/2

        state.mat = S

        return SE3[2,6](state)

    def makePhi(self, u, dt, state, error):
        if error == ERROR.LEFT:
            zero    = np.zeros((3,3))
            I       = np.eye(3)


            w_cross = SO3.wedge( u[:3] - state.aug[:3] )
            a_cross = SO3.wedge( u[3:] - state.aug[3:] )
            R = state.R.mat
            A = np.block([[-w_cross,     zero,     zero,   -I, zero],
                        [-R@a_cross,     zero,     zero, zero,   -R],
                        [      zero,        I,     zero, zero, zero],
                        [      zero,     zero,     zero, zero, zero],
                        [      zero,     zero,     zero, zero, zero]])

        elif error == ERROR.RIGHT:
            zero    = np.zeros((3,3))
            I       = np.eye(3)


            R = state.R.mat
            w_cross = SO3.wedge( u[:3] - state.aug[:3] )
            a_temp = SO3.wedge( R@(u[3:] - state.aug[3:]) )
            A = np.block([[    zero,     zero,     zero,   -R, zero],
                        [   -a_temp,     zero,     zero, zero,   -R],
                        [      zero,        I,     zero, zero, zero],
                        [      zero,     zero,     zero, zero, zero],
                        [      zero,     zero,     zero, zero, zero]])

        return expm(A*dt)


class VelInertialSE3QEKF:
    def __init__(self, M, left=True):
        self.M = M
        self.left = left

    def H(self, state, error):
        R = state.R.mat
        v_hat = state[0]
        zero = np.zeros((3,3))

        if error == ERROR.LEFT:
            H = np.block([SO3.wedge(R.T@v_hat), R.T, zero, zero, zero])
        elif error == ERROR.RIGHT:
            H = np.block([R.T@SO3.wedge(v_hat), R.T, zero, zero, zero])

        return H

    def calcV(self, state, z):
        return z[:3] - state.R.mat.T@state[0]

class DepthSE3QEKF:
    def __init__(self, M):
        self.M = M

    def H(self, state, error):
        H = np.zeros((1,15))
        H[0,8] = 1
        return H

    def calcV(self, state, z):
        return z[:1] - state[1][-1]

#------------------- QEKF FILTER ----------------#
class QEKF:
    def __init__(self, process, start, error=ERROR.RIGHT):
        self.state = start
        self.pModel = process
        self.mModels = {}
        self.n = self.state.cov.shape[0]
        self.aug_size = self.state.aug.shape[0]

        self.error = error

    def predict(self, u, dt):
        Phi = self.pModel.makePhi(u, dt, self.state, self.error)
        self.state.cov = Phi@self.state.cov@Phi.T + self.pModel.Q*dt

        self.state = self.pModel.f(u, dt, self.state)

        return self.state

    def addMeasureModel(self, name, mm):
        self.mModels[name] = mm

    def update(self, name, z):
        mm = self.mModels[name]

        H = mm.H(self.state, self.error)
        V = mm.calcV(self.state, z)
        S = H@self.state.cov@H.T + mm.M

        K = self.state.cov@H.T@np.linalg.inv(S)
        KV = K@V

        state = self.state.mat.copy()

        if self.state.R.mat.shape == (2,2):
            state[:2,:2] = SO2.exp([KV[0]]).mat@state[:2,:2]
            for i in range(state.shape[0]-2):
                state[:2,2+i] += KV[1+i*2:3+i*2]

        elif self.state.R.mat.shape == (3,3):
            dtheta = SO3.exp(KV[:3]).mat
            if self.error == ERROR.LEFT:
                state[:3,:3] = state[:3,:3]@dtheta
            elif self.error == ERROR.RIGHT:
                state[:3,:3] = dtheta@state[:3,:3]

            for i in range(state.shape[0]-3):
                state[:3,3+i] += KV[3+i*3:6+i*3]

        if self.aug_size != 0:
            self.state.aug = self.state.aug + KV[-self.aug_size:]

        self.state.mat = state
        self.state.cov = (np.eye(self.n) - K@H)@self.state.cov

        return self.state



