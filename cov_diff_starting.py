import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from inekf import SE2
from helpers.misc import setup_figure, mscatter, multivariate_lie_normal

np.set_printoptions(suppress=True)

def main():
    ######## SETUP CONSTANTS
    n = 9
    num = 250
    xs = np.linspace(-10, 10, n)
    std = np.diag([0.5*np.pi/180, .01, .01])


    ####### GATHER DATA
    r_samples = np.zeros((n, num, 3))
    l_samples = np.zeros((n, num, 3))
    for i,x in enumerate(xs):
        r_samples[i] = multivariate_lie_normal(SE2(0, x, 0), std, num, right=True)[0]
        l_samples[i] = multivariate_lie_normal(SE2(0, x, 0), std, num, right=False)[0]
    r_samples = r_samples.reshape(-1, 3)
    l_samples = l_samples.reshape(-1, 3)


    ######## SETUP FIGURE
    c = setup_figure()
    fig, ax = plt.subplots(1,2, figsize=(8.5,1.5), sharey=True)


    ###### PLOT
    mscatter(r_samples[:,1], r_samples[:,2], r_samples[:,0], ax=ax[1], marker='|', s=30, color=c[0])
    # mscatter(r_samples[:,1], r_samples[:,2], r_samples[:,0], ax=ax[0], marker='^', s=5,  color=c[0])
    mscatter(l_samples[:,1], l_samples[:,2], l_samples[:,0], ax=ax[0], marker='|', s=30, color=c[1])
    # mscatter(l_samples[:,1], l_samples[:,2], l_samples[:,0], ax=ax[1], marker='^', s=5,  color=c[1])

    for a in ax:
        a.set_aspect(1.0)
        a.set_xlim([-12,12])
        a.set_ylim([-3,3])
        a.set_xlabel("X", weight='bold')
        a.tick_params('x', pad=-2)
        a.tick_params('y', pad=-2)

    ax[0].set_title('Left Error Distribution', weight='bold')
    ax[1].set_title('Right Error Distribution', weight='bold')
    ax[0].set_ylabel("Y", weight='bold')

    plt.tight_layout()
    # plt.savefig(f'figures/covariance.pdf', dpi=200)
    plt.show()

if __name__ == "__main__":
    main()